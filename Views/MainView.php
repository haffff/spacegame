<?php
if(is_null($this->model->result ))
{
    header("Location: /createShip");
}
?>

<div style="text; background-color:#393939;margin-top:10px;">
    <table>
        <tr>
            <td>
                Ship Name:
            </td>
            <td>
                <?php $this->result->model->shipname ?>
            </td>
        </tr>
        <tr>
            <td>
                Health: <?php echo $this->model->result->hp . "/" . $this->model->result->maxhp ?>
            </td>
            <td>
                <progress value=<?php echo $this->model->result->hp ?> max=<?php echo $this->model->result->maxhp ?>> </progress>
            </td>
        </tr>
        <tr>
            <td>
                Fuel:<?php echo $this->model->result->fuel . "/" . $this->model->result->maxfuel ?>
            </td>
            <td>
                <progress value=<?php echo $this->model->result->fuel ?> max=<?php echo $this->model->result->maxfuel ?>> </progress>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</div>