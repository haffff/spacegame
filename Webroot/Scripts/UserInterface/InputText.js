var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InputText = function (_React$Component) {
    _inherits(InputText, _React$Component);

    function InputText(props) {
        _classCallCheck(this, InputText);

        var _this = _possibleConstructorReturn(this, (InputText.__proto__ || Object.getPrototypeOf(InputText)).call(this, props));

        _this.state = { textcolor: 'powderblue', color: '#6833F6', value: "" };
        return _this;
    }

    _createClass(InputText, [{
        key: 'render',
        value: function render() {
            var _this2 = this;

            return React.createElement(
                'div',
                null,
                React.createElement(
                    'label',
                    null,
                    this.props.title
                ),
                React.createElement('br', null),
                React.createElement('input', { style: { padding: 5, backgroundColor: this.state.color, color: this.state.textcolor, borderStyle: 'none', value: this.state.value },
                    type: this.props.password === undefined ? "text" : "password",
                    name: this.props.name,
                    onMouseEnter: function onMouseEnter() {
                        _this2.setState({ border: _this2.state.border, color: '#8888FF', value: _this2.state.value });
                    },
                    onMouseOut: function onMouseOut() {
                        _this2.setState({ border: _this2.state.border, color: '#6833F6', value: _this2.state.value });
                    },
                    onChange: function onChange(val) {
                        _this2.setState({ border: _this2.state.border, color: _this2.state.color, value: val.target.value });
                    },
                    onBlur: function onBlur() {
                        if (_this2.props.check !== undefined) {
                            var result = _this2.props.check(_this2.state.value);
                            if (result.success) {
                                _this2.setState({ textcolor: 'green', color: _this2.state.color });
                            } else {
                                _this2.setState({ textcolor: 'red', color: _this2.state.color });
                            }
                        }
                    }
                })
            );
        }
    }]);

    return InputText;
}(React.Component);