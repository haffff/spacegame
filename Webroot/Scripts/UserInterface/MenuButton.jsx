
class MenuButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = { color: '#4611E4' };
    }

    render() {
        return (
            <button
                onMouseEnter={() => { this.setState({ color: '#6833F6' }) }}
                onMouseOut={() => { this.setState({ color: '#4611E4' }) }}
                style={
                    {
                        fontSize: 18,
                        width: (this.props.width === undefined) ? "90%" : this.props.width,
                        padding: 10,
                        margin: 5,
                        textAlign: 'left',
                        verticalAlign: 'center',
                        backgroundColor: this.state.color,
                        border: 'none',
                        color: 'powderblue'
                    }
                }
                onClick={() => { this.props.location===undefined?this.props.function():window.location.href = this.props.location }}>
                <img style={{}} src={this.props.icon_src} />
                {this.props.value}
            </button>
        );
    }
}
