
class InputText extends React.Component {

    constructor(props) {
        super(props);
        this.state = { textcolor: 'powderblue', color: '#6833F6', value: "" };
    }

    render() {
        return (
            <div>
                <label>{this.props.title}</label><br />
                <input style={{ padding: 5, backgroundColor: this.state.color, color: this.state.textcolor, borderStyle: 'none', value: this.state.value }}
                    type={this.props.password === undefined ? "text" : "password"}
                    name={this.props.name}
                    onMouseEnter={() => { this.setState({ border: this.state.border, color: '#8888FF', value: this.state.value }) }}
                    onMouseOut={() => { this.setState({ border: this.state.border, color: '#6833F6', value: this.state.value }) }}
                    onChange={(val) => { this.setState({ border: this.state.border, color: this.state.color, value: val.target.value }) }}
                    onBlur={() => {
                        if (this.props.check !== undefined) {
                            let result = this.props.check(this.state.value)
                            if (result.success) {
                                this.setState({ textcolor: 'green', color: this.state.color });
                            }
                            else {
                                this.setState({ textcolor: 'red', color: this.state.color });
                            }
                        }
                    }}
                />
            </div>
        );
    }
}