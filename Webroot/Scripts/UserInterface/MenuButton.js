var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MenuButton = function (_React$Component) {
    _inherits(MenuButton, _React$Component);

    function MenuButton(props) {
        _classCallCheck(this, MenuButton);

        var _this = _possibleConstructorReturn(this, (MenuButton.__proto__ || Object.getPrototypeOf(MenuButton)).call(this, props));

        _this.state = { color: '#4611E4' };
        return _this;
    }

    _createClass(MenuButton, [{
        key: 'render',
        value: function render() {
            var _this2 = this;

            return React.createElement(
                'button',
                {
                    onMouseEnter: function onMouseEnter() {
                        _this2.setState({ color: '#6833F6' });
                    },
                    onMouseOut: function onMouseOut() {
                        _this2.setState({ color: '#4611E4' });
                    },
                    style: {
                        fontSize: 18,
                        width: this.props.width === undefined ? "90%" : this.props.width,
                        padding: 10,
                        margin: 5,
                        textAlign: 'left',
                        verticalAlign: 'center',
                        backgroundColor: this.state.color,
                        border: 'none',
                        color: 'powderblue'
                    },
                    onClick: function onClick() {
                        _this2.props.location === undefined ? _this2.props.function() : window.location.href = _this2.props.location;
                    } },
                React.createElement('img', { style: {}, src: this.props.icon_src }),
                this.props.value
            );
        }
    }]);

    return MenuButton;
}(React.Component);