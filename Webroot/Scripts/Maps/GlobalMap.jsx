"use strict";



class GlobalMap extends React.Component {

    points = null;
    target = 0;

    constructor(props) {
        super(props);
        this.state = {
             playerData: null,
             firstTime:true,
             mouseDown: false,
             originalPos: null,
             tmpOffsetX:0 ,
             tmpOffsetY:0,
             offsetX: 0,
             offsetY: 0,
             zoom: 10,
             informationShow:false,
             informationShowJump:false }
    }

    componentDidMount() {
        this.ctx = this.refs.canvas.getContext('2d');
        //this.drawPoints(ctx);
    }



    loadPoints(ctx) {
        $.get("globalmap/getmapdata", (mapdata) => {
            $.get("globalmap/getplayerdata", (playerdata) => {
                let object = JSON.parse(mapdata);
                this.state.playerData = JSON.parse(playerdata);

                

            this.points = object;

            if (this.state.firstTime)
            {
            this.state.tmpOffsetX = this.state.offsetX = -(this.state.playerData.PlayerPosition.x * this.state.zoom) + ($("#rightpanel").width() / 2) ;
            this.state.tmpOffsetY = this.state.offsetY = -(this.state.playerData.PlayerPosition.y * this.state.zoom) + ($("#rightpanel").height() / 2);

            
            
            this.state.firstTime = false;
            }

            object = object.map(element => {
                this.ctx.fillStyle = 'rgb(100,100,100)';
                if (this.state.playerData.PlayerPosition.x == element.x && this.state.playerData.PlayerPosition.y == element.y) {
                    element["category"] = "player";
                    this.ctx.fillStyle = 'lightgreen'; 
                }else
                        if (Math.pow(element.x - this.state.playerData.PlayerPosition.x, 2)
                            + Math.pow(element.y - this.state.playerData.PlayerPosition.y, 2) <= Math.pow(this.props.value, 2))
                {
                    element["category"] = "reachable";
                    this.ctx.fillStyle = 'rgb(255,255,0)';
                }else
                {
                    element["category"] = "none";
                }

                this.state.playerData.PlayerSystems.forEach((e)=>{
                    if(e.systemid == element.id)
                    {
                        
                        if(element.category == "reachable")
                        {
                            element.category = "knownAndReachable";
                            this.ctx.fillStyle = 'rgb(100,100,255)';
                        }else if(element.category == "none")
                        {
                            element.category = "known";
                            
                            this.ctx.fillStyle = 'rgb(255,255,255)';
                        }
                    }
                });

                this.ctx.fillRect((element.x) * this.state.zoom + this.state.offsetX, (element.y) * this.state.zoom + this.state.offsetY, 4, 4);
            
            });
            
                this.ctx.strokeStyle = 'lightgreen';
                this.ctx.beginPath();
                this.ctx.arc((this.state.playerData.PlayerPosition.x) * this.state.zoom + this.state.offsetX + 2, (this.state.playerData.PlayerPosition.y) * this.state.zoom + this.state.offsetY + 2, 10, 0, 2 * Math.PI);
                this.ctx.stroke();
        });

        }
        );
    }

    componentWillMount()
    {
        this.loadPoints(this.ctx);
    }

    drawPoints() {
        this.ctx.clearRect(0, 0, $("#rightpanel").width(), $("#rightpanel").height());
        this.points.forEach(element => {
            
            switch(element.category)
            {
                case "player":
                    this.ctx.fillStyle = 'lightgreen';
                    this.ctx.beginPath();
                    this.ctx.arc((element.x) * this.state.zoom + this.state.tmpOffsetX + 2, (element.y) * this.state.zoom + this.state.tmpOffsetY + 2, 10, 0, 2 * Math.PI);
                    this.ctx.stroke();
                break;
                case "reachable":
                    this.ctx.fillStyle = 'rgb(255,255,0)';
                break;
                case "known":
                    this.ctx.fillStyle = 'rgb(255,255,255)';
                break;
                case "knownAndReachable":
                    this.ctx.fillStyle = 'rgb(100,100,255)';
                    break;
                default:
                    this.ctx.fillStyle = 'rgb(100,100,100)';
            }
            this.ctx.fillRect((element.x) * this.state.zoom + this.state.tmpOffsetX, (element.y) * this.state.zoom + this.state.tmpOffsetY, 4, 4);
        });
    }

    mouseUp() {

    }

    infoDisplay(x,y, category, systemid)
    {
        this.state.target = systemid;
        $.get("/globalmap/getSystemData/" + systemid, (data)=>{
            let object = JSON.parse(data);
            this.state.informationShow = true;
            this.state.informationX = x;
            this.state.informationY = y;
            this.state.informationMessage = object[0];
                if (category == "reachable"|| category == "knownAndReachable") {
                this.state.informationShowJump = true;
            } else {
                this.state.informationShowJump = false;
            }
            this.setState(this.state);

            this.drawPoints();
        });
        
    }

    playerJump(target)
    {
        $.get("/globalmap/flyToOtherSystem/"+this.state.target,function(result){

            if(JSON.parse(result))
            {
                window.location.href = "/globalmap";
            }
            else
            {
                alert("dundundun");
            }
        });
    }

    render() {

        
        
        let information = (<InformationWindow 
            value={this.state.informationMessage} 
            buttontitle={this.state.informationShowJump ? "Jump" : "Okay"}
            function={()=>{this.playerJump(this.state.target)}}
            x={this.state.informationX}
            y={this.state.informationY} 
            />)
        if(!this.state.informationShow)
        {
            information = null;
        }
        return (
            <div>
            <canvas ref="canvas"
                width={$("#rightpanel").width()}
                height={$("#rightpanel").height()}
                style={{ backgroundColor: 'black' }}
                onMouseDown={(e) => {
                    
                    this.state.mouseDown = true;
                    this.state.originalPos = { x: e.clientX, y: e.clientY };
 
                    let PosX = ((e.clientX - $("#rightpanel").offset().left) - this.state.offsetX) / this.state.zoom;
                    let PosY = ((e.clientY - $("#rightpanel").offset().top) - this.state.offsetY) / this.state.zoom;
                    if(isNaN(PosX) || isNaN(PosY))
                    {
                        return;
                    }
                    
                    
                    let clicked = false;
                    
                    this.points.forEach(element => {
                        if (element.x == Math.round(PosX) && element.y == Math.round(PosY)) {
                            
                            clicked = true;
                            let x = e.clientX;
                            let y = e.clientY;
                            this.infoDisplay(x,y,element.category,element.id);
                        }
                    });

                    if(!clicked)
                    {
                    this.state.informationShow = false;
                    this.setState(this.state);
                    }
    }}
                onMouseUp={() => {

                    this.state.mouseDown = false;
                    this.state.offsetX = this.state.tmpOffsetX;
                    this.state.offsetY = this.state.tmpOffsetY;

                }}
                onMouseOut={() => {

                    this.state.mouseDown = false;
                    this.state.offsetX = this.state.tmpOffsetX;
                    this.state.offsetY = this.state.tmpOffsetY
                }}

                onMouseMove={
                    (e) => {
                        if (this.state.mouseDown) {


                            let newX = e.clientX;
                            let newY = e.clientY;

                            this.state.newPos = { x: newX, y: newY };
                            
                            this.state.tmpOffsetX = this.state.offsetX + (newX - this.state.originalPos.x);
                            this.state.tmpOffsetY = this.state.offsetY + (newY - this.state.originalPos.y);

                            this.drawPoints();

                        }
                    }
                } >
            </canvas>
                {information}</div>
            
        );
    }



}
