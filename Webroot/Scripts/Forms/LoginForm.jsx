
    class LoginForm extends React.Component {
        render() {

            return (
                <form style={{ backgroundColor: '#393939' }} method="post" action={this.props.action} >
                    <InputText title="Login" name="login" />
                    <InputText title="Password" password name="password" /*check={(content) => { $.get("/login/islogintaken/"+content) alert(content) }}*/ />
                    <SubmitButton value="Login" width="20%" />
                </form>
            );
        }
    }
