
class RegisterForm extends React.Component {
    render() {
        return (
            <form style={{ backgroundColor: '#393939' }} method="post" action={this.props.action} >
                <div style={{ margin: 10 }}>
                    <InputText title="Login" name="login" />
                    <InputText title="Password" password name="password" />
                    <InputText title="Email" name="email" />
                </div>
                <SubmitButton value="Register" width="20%" />
            </form>
        );
    }
}