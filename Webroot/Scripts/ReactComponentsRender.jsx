"use strict";

    if (document.getElementById('guestLeftPanel') !== null) {
        ReactDOM.render(<div><MenuButton value="Zaloguj" location="/login" icon_src="/Webroot/Assets/Icons/identity.png" /><br />
        <MenuButton value="Zarejestruj" location="/register" icon_src="/Webroot/Assets/Icons/register.png" /></div>, document.getElementById('guestLeftPanel'));
    }

    if (document.getElementById('loggedLeftPanel') !== null) {
        ReactDOM.render(<div><MenuButton value="Wyloguj" location="/login/logout" icon_src="/Webroot/Assets/Icons/identity.png" /><br />
        </div>, document.getElementById('loggedLeftPanel'));
    }



    if(document.getElementById("loginform") !== null) 
    {
        ReactDOM.render(<LoginForm action="/login/send" />, document.getElementById("loginform"));
    }


    if (document.getElementById("registerform") !== null) {
        ReactDOM.render(<RegisterForm action="/register/send" />, document.getElementById("registerform"));
    }

    if (document.getElementById('globalMap') !== null) {
        let element = document.getElementById('globalMap');
        ReactDOM.render(<GlobalMap value={element.getAttribute("value")}/>, element);
    }







