"use strict";

if (document.getElementById('guestLeftPanel') !== null) {
    ReactDOM.render(React.createElement("div", null, React.createElement(MenuButton, { value: "Zaloguj", location: "/login", icon_src: "/Webroot/Assets/Icons/identity.png" }), React.createElement("br", null), React.createElement(MenuButton, { value: "Zarejestruj", location: "/register", icon_src: "/Webroot/Assets/Icons/register.png" })), document.getElementById('guestLeftPanel'));
}

if (document.getElementById('loggedLeftPanel') !== null) {
    ReactDOM.render(React.createElement("div", null, React.createElement(MenuButton, { value: "Wyloguj", location: "/login/logout", icon_src: "/Webroot/Assets/Icons/identity.png" }), React.createElement("br", null)), document.getElementById('loggedLeftPanel'));
}

if (document.getElementById("loginform") !== null) {
    ReactDOM.render(React.createElement(LoginForm, { action: "/login/send" }), document.getElementById("loginform"));
}

if (document.getElementById("registerform") !== null) {
    ReactDOM.render(React.createElement(RegisterForm, { action: "/register/send" }), document.getElementById("registerform"));
}

if (document.getElementById('globalMap') !== null) {
    var element = document.getElementById('globalMap');
    ReactDOM.render(React.createElement(GlobalMap, { value: element.getAttribute("value") }), element);
}