<?php
session_start();

require_once "../Config/GameSettings.php";

require_once "../Models/Model.php";
require_once "../Controllers/Controller.php";
require_once "../Views/View.php";

$uri = $_SERVER['REQUEST_URI'];
$array = explode("/", $uri);
require_once "../Controllers/" . ucfirst($array[1]) . "Controller.php";
require_once "../Models/" . ucfirst($array[1]) . "Model.php";

$Model_string = ucfirst($array[1]) . "Model";
$Controller_string = ucfirst($array[1]) . "Controller";


$Model = new $Model_string();
$Controller = new $Controller_string($Model, ucfirst($array[1]));

if (count($array) > 2) {
    $func = $array[2];
    if (count($array) > 3)
        
        if(count($array)>4){
            $Controller->$func($array[3],$array[4]);
        }
        else {
        $Controller->$func($array[3]);
        }
    else {
        $Controller->$func();
    }
}
if ($Controller->nopage) {
    $Controller->Render();
    return;
}
?>


<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Gra</title>

    <link rel="stylesheet" type="text/css" href="/Webroot/Styles/MainStyle.css" />
    <link rel="stylesheet" type="text/css" href="/Webroot/Styles/UserInterface.css" />

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

</head>

<body>
    <div id="toppanel">

    </div>
    <div id="leftmenu">
        <?php
        $Controller->RenderLeftPanel();
        ?>
    </div>
    <div id="rightpanel">


        <?php

        $Controller->Render();
        ?>
    </div>

    <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
    

    <script src="/Webroot/Scripts/UserInterface/MenuButton.js"></script>
    <script src="/Webroot/Scripts/UserInterface/SubmitButton.js"></script>
    <script src="/Webroot/Scripts/UserInterface/InputText.js"></script>
    <script src="/Webroot/Scripts/UserInterface/InformationWindow.js"></script>

    <script src="/Webroot/Scripts/Forms/LoginForm.js"></script>
    <script src="/Webroot/Scripts/Forms/RegisterForm.js"></script>

    <script src="/Webroot/Scripts/Maps/GlobalMap.js"></script>

    <script src="/Webroot/Scripts/ReactComponentsRender.js"></script>
</body>

</html>