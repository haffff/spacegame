"use strict";

var _typeof9 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _typeof8 = typeof Symbol === "function" && _typeof9(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof9(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof9(obj);
};

var _typeof7 = typeof Symbol === "function" && _typeof8(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof8(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof8(obj);
};

var _typeof6 = typeof Symbol === "function" && _typeof7(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof7(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof7(obj);
};

var _typeof5 = typeof Symbol === "function" && _typeof6(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof6(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof6(obj);
};

var _typeof4 = typeof Symbol === "function" && _typeof5(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof5(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof5(obj);
};

var _typeof3 = typeof Symbol === "function" && _typeof4(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof4(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof4(obj);
};

var _typeof2 = typeof Symbol === "function" && _typeof3(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof3(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof3(obj);
};

var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
};

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _possibleConstructorReturn(self, call) {
    if (!self) {
        throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var GlobalMap = function (_React$Component) {
    _inherits(GlobalMap, _React$Component);

    function GlobalMap(props) {
        _classCallCheck(this, GlobalMap);

        var _this = _possibleConstructorReturn(this, (GlobalMap.__proto__ || Object.getPrototypeOf(GlobalMap)).call(this, props));

        _this.state = { mouseDown: false, points: null, originalPos: null, offsetX: 0, offsetY: 0, zoom: 10 };

        return _this;
    }

    _createClass(GlobalMap, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            this.ctx = this.refs.canvas.getContext('2d');
            //this.drawPoints(ctx);
        }
    }, {
        key: "loadPoints",
        value: function loadPoints(ctx) {
            var _this2 = this;

            $.get("globalmap/getmapdata", function (data) {
                var object = JSON.parse(data);

                _this2.ctx.fillStyle = 'rgb(255,255,255)';

                _this2.state.points = object;

                object.forEach(function (element) {
                    _this2.ctx.fillRect(element.x * _this2.state.zoom + _this2.state.offsetX, element.y * _this2.state.zoom + _this2.state.offsetY, 4, 4);
                });
            });
        }
    }, {
        key: "drawPoints",
        value: function drawPoints() {
            var _this3 = this;

            this.ctx.clearRect(0, 0, $("#rightpanel").width(), $("#rightpanel").height());

            this.ctx.fillStyle = 'rgb(255,255,255)';

            this.state.points.forEach(function (element) {

                _this3.ctx.fillRect(element.x * _this3.state.zoom + _this3.state.tmpOffsetX, element.y * _this3.state.zoom + _this3.state.tmpOffsetY, 4, 4);
            });
        }
    }, {
        key: "mouseUp",
        value: function mouseUp() {}
    }, {
        key: "render",
        value: function render() {
            var _this4 = this;

            this.loadPoints(this.ctx);

            return React.createElement("canvas", { ref: "canvas",
                width: $("#rightpanel").width(),
                height: $("#rightpanel").height(),
                style: { backgroundColor: 'black' },
                onMouseDown: function onMouseDown(e) {
                    _this4.state.mouseDown = true;
                    _this4.state.originalPos = { x: e.clientX, y: e.clientY };

                    var PosX = (e.clientX - $("#rightpanel").offset().left - _this4.state.offsetX) / _this4.state.zoom;
                    var PosY = (e.clientY - $("#rightpanel").offset().top - _this4.state.offsetY) / _this4.state.zoom;

                    _this4.state.points.forEach(function (element) {
                        if (element.x == Math.round(PosX) && element.y == Math.round(PosY)) {

                            _this4.setState(_this4.state);
                        }
                    });
                },
                onMouseUp: function onMouseUp() {

                    _this4.state.mouseDown = false;
                    _this4.state.offsetX = _this4.state.tmpOffsetX;
                    _this4.state.offsetY = _this4.state.tmpOffsetY;
                },
                onMouseOut: function onMouseOut() {

                    _this4.state.mouseDown = false;
                    _this4.state.offsetX = _this4.state.tmpOffsetX;
                    _this4.state.offsetY = _this4.state.tmpOffsetY;
                },

                onMouseMove: function onMouseMove(e) {
                    if (_this4.state.mouseDown) {

                        var newX = e.clientX;
                        var newY = e.clientY;

                        _this4.state.newPos = { x: newX, y: newY };

                        _this4.state.tmpOffsetX = _this4.state.offsetX + (newX - _this4.state.originalPos.x);
                        _this4.state.tmpOffsetY = _this4.state.offsetY + (newY - _this4.state.originalPos.y);
                        //console.log(this.state.tmpOffsetX + " " + this.state.tmpOffsetY);

                        _this4.drawPoints();
                    }
                } });
        }
    }]);

    return GlobalMap;
}(React.Component);