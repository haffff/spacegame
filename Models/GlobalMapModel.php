<?php

class GlobalMapModel extends Model
{
    public function getMapData($userid)
    {
        $result = $this->db->Query_To_Array("SELECT * FROM coords",array());
        $this->result = json_encode($result);
    }


    public function getPlayerData($userid)
    {
        $playerpos = $this->db->Query_First_Row("SELECT x,y FROM playerships INNER JOIN coords ON coords.id = playerships.systemid WHERE userid = ?", array($userid));
        $playerknowledge = $this->db->Query_To_Array("SELECT * FROM playersystemsknowledge WHERE id = ?" , array($userid));

        
        $this->result = json_encode(array("PlayerPosition"=>$playerpos,"PlayerSystems"=>$playerknowledge));
    }

    public function flyToOtherSystem($userid, $target)
    {
        if($this->compareSystems($userid, $target)&&
           $this->db->Query_Has_Result("SELECT fuel FROM playerships WHERE userid = ? AND fuel > 0", array($userid))
        )
        {
        $this->db->ExecuteStatement("UPDATE playerships SET fuel = fuel - 1 , systemid = ? WHERE userid = ?",array($target,$userid));
        $this->db->ExecuteStatement("INSERT IGNORE INTO playersystemsknowledge VALUES (?,?)",array($userid,$target));
        $this->result = json_encode(true);
    }else{
            $this->result = json_encode(false);
        }
    }

    public function compareSystems($userid , $target)
    {
        $center = $this->db->Query_First_Row("SELECT x,y FROM users INNER JOIN playerships ON users.id = playerships.userid INNER JOIN coords ON playerships.systemid = coords.id WHERE userid = ?", array($userid));
        $point = $this->db->Query_First_Row("SELECT x,y FROM coords WHERE id = ?",array($target));

        if( pow($point['x'] - $center['x'],2) + pow($point['y'] - $center['y'], 2) <= pow(MAX_TRAVEL_DISTANCE,2))
        {
            return true;
        }else 
        {
            return false;
        }
    }

    public function getSystemData($userid,$system)
    {
        if($this->db->Query_Has_Result("SELECT systemid FROM playersystemsknowledge WHERE id = ? AND systemid = ?", array($userid, $system)))
        {
            $this->result = json_encode(array("Tutaj będą info o systemie","i tu tez xd"));
        }
        else
        {
            $this->result = json_encode(array("Unknown"));
        }
    }

}
?>