<?php

class RegisterModel extends Model
{
    private $success;
    private $errorMessage;

    public function IsSuccess()
    {
        return $this->success;
    }
    public function GetErrorMessage()
    {
        return $this->errorMessage;
    }

    public function register(string $login,string $passwd,string $email)
    {

        if($login == "" || $passwd == "" || $email == "")
        {
            $this->success = false;
            $this->errorMessage = "Field cannot be empty";
            return;
        }

       $res = $this->db->Query_First_Row_First_Value("SELECT id FROM login=?", $login);
       if(isset($res))
       {
           $this->success = false;
           $this->errorMessage = "Login is already taken";
           return;
       }

       $res = $this->db->Query_First_Row_First_Value("SELECT id FROM email=?", $email);

       if (isset($res)) {
            $this->success = false;
            $this->errorMessage = "Email is already taken";
            return;
        }


        
        $password = password_hash($passwd, PASSWORD_DEFAULT);
        
        $res = $this->db->ExecuteStatement("INSERT INTO users VALUES (null,?,?,0,?);",$login,$password,$email);
        
        $this->success = true;
    }
}

?>