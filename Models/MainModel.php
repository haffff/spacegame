<?php

class MainModel extends Model
{
    public function getPlayerData($userid)
    {
        $result = $this->db->Query_First_Row("SELECT playerships.id as shipid, name , hp , maxhp, fuel, maxfuel, status ,contactmsg FROM playerships INNER JOIN shiptypes ON playerships.shiptype = shiptypes.id WHERE playerships.userid = ?", array($userid));
        
        if(!empty($result))
        {
            $this->result = $result;
        }  
        else 
        {
            $this->result = null;
        }
    }


    public function debug()
    {
        $this->db->ExecuteStatement("SET FOREIGN_KEY_CHECKS = 0;");
        $this->db->ExecuteStatement("DROP TABLE `coords`, `planets`, `planettypes`, `playerships`, `playersystemsknowledge`, `shiptypes`, `users`;");
        $this->db->ExecuteStatement("SET FOREIGN_KEY_CHECKS = 1;");
        require_once "../Engine/Init.php";
    }
}
?>