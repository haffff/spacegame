<?php

class LoginModel extends Model
{
    private $CheckPasswordResult = null;
    private $CheckLoginResult = null;

  public function CheckLogin(string $login)
  {
        $this->CheckLoginResult = $this->db->Query_Has_Result("SELECT id FROM users WHERE login = ?", array($login));
  }

  public function CheckPassword(string $login, string $password)
  {
      $row = $this->db->Query_First_Row("SELECT id,password,admin FROM users WHERE login = ?", array($login));
    
      if(password_verify($password,$row['password']))
      {
        $this->result = $row;
        $this->CheckPasswordResult = true;
      }
      else{
      $this->CheckPasswordResult = false;
      }
  }

  public function GetCheckLoginResult()
  {
    return $this->CheckLoginResult;
  }

   public function GetCheckPasswordResult()
   {
    return $this->CheckPasswordResult;
   }

    public function GetResult()
    {
        return $this->result;
    }

}
?>