<?php

class RegisterCheckModel extends Model
{
    public $result = null;
    public function login(string $value)
    {
        $result = !$this->db->Query_Has_Result("SELECT id FROM users WHERE login = ?",array($value));
        $this->result = json_encode(array("success"=> $result,"message"=>$result?"":"Login is already taken"));
    }
    public function email(string $value)
    {
        $result = !$this->db->Query_Has_Result("SELECT id FROM users WHERE email = ?", array($value));
        $this->result = json_encode(array("success" => $result, "message" => $result ? "" : "Email is already taken"));
    }
}
?>