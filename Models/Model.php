<?php
require_once "../Engine/Database.php";
class Model
{
protected $db = null;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function __destruct()
    {
        $this->db = null;
    }
}

?>