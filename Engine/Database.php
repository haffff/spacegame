<?php 

require_once "../Config/DatabaseConfig.php";

class Database {

private $result = null;
private $statements = array();

public function __construct()
{
    try{
        $this->connection = new PDO("mysql:host=".constant('DB_HOST'). ";dbname=" . constant('DB_BASE').";port=".constant('DB_PORT'),constant('DB_USERNAME'),constant('DB_PASSWORD'));
        
    }catch(PDOException $e)
    {
        echo "Connection with db failed. Error: ".$e->getMessage();
    }
}

public function __destruct()
{
    $this->connection = null;
}

public function AddToStatementList(string $name,string $statement)
{
    $this->statements[$name] = $statement;
}

public function ExecuteStatementFromList(string $name, ...$args)
{
    $this->ExecuteStatement($this->statements[$name],$args);
}

public function ExecuteStatement(string $statement, ...$args)
{
        if(is_null($args))
        {
            $args = array();
        }

        //Na wypadek jakby ktoś wrzucił argumenty jako tablice
        if(!empty($args))
        {
            if(is_array($args[0]))
            {
                $args = $args[0];
            }
        }

        $stmt = null;
        if (isset($this->connection)) {
            $stmt = $this->connection->prepare($statement);

        for ($i = 0; $i < count($args); $i++) {
            $stmt->bindParam($i + 1, $args[$i]);
        }

        try{
        $stmt->execute();
        } catch (PDOException $e) {
            echo "Statement failed. Error: " . $e->getMessage();
        }
    }
}

 public function UnsafeExecuteStatement(string $statement)
 {
    try {
        $result = $this->connection->exec($statement);
    } catch (PDOException $e) {
        echo "Statement failed. Error: " .  $e->getMessage();
    }

    if($result == false)
    {
        die("Bad statement");
    }


    
 }

public function Query_Has_Result(string $query, $args)
{
    $result = $this->Query($query,$args);
    $array = $result->fetchAll();
    $result = null;
    
    if(empty($array))
    {
           
    return false;
    }
        
    return true;
}

public function Query_To_Array(string $query, $args)
{
    $result = $this->Query($query,$args);
    $array = $result->fetchAll(PDO::FETCH_ASSOC);
    $result = null;
    return $array;
}

public function Query_First_Row(string $query, $args)
{
    $result = $this->Query($query,$args);
    $array = $result->fetch();
    $result = null;
    return $array;
}

public function Query_First_Row_First_Value(string $query, $args)
{
        $result = $this->Query($query,$args);
        $array = $result->fetch()[0][0];
        $result = null;
        return $array;
}

private function Query(string $query, $args)
{
    if (isset($this->connection)) {

        $result = null;
        try{
            $query = $this->connection->prepare($query);
            $query->execute($args);
            $result = $query;
            
            } catch (PDOException $e){
            echo "Query failed. Error: " .  $e->getMessage();
        }

        if($result == false)
        {
            die("Bad query");
        }
        
        return $result;
        }
    }
} 
?>