<?php 

        require_once "Database.php";
        require_once "../Config/WorldSettings.php";

        $db = new Database();

        //Tworzenie Tabel

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS coords (id INT NOT NULL PRIMARY KEY auto_increment , x int NOT NULL, y int NOT NULL);",array());
        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS users 
            (id INT NOT NULL PRIMARY KEY auto_increment ,
            login VARCHAR(30) NOT NULL,
            password VARCHAR(255) NOT NULL,
            admin INT(1) not null,
            email varchar(40) not null);", array());

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS playersystemsknowledge 
            (id INT NOT NULL, systemid INT NOT NULL
            );", array());


        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS playerships 
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            userid INT NOT NULL,
            shiptype INT NOT NULL,
            systemid INT NOT NULL,
            hp INT NOT NULL,
            fuel INT NOT NULL,
            status varchar(10),
            contactmsg varchar(128));", array());

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS installedweapons 
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            shipid INT NOT NULL,
            type INT NOT NULL)", array());

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS installedutilities
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            shipid INT NOT NULL,
            type INT NOT NULL)", array());

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS utilitytypes
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            action INT NOT NULL,
             INT NOT NULL)", array());

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS weapontypes
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            shipid INT NOT NULL,
            type INT NOT NULL)", array());

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS shiptypes 
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            name varchar(20) NOT NULL,
            maxweaponslots INT NOT NULL,
            maxutilityslots INT NOT NULL,
            maxinventory INT NOT NULL,
            maxhp INT NOT NULL,
            baseprice INT NOT NULL)", array());


        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS planets 
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            systemid INT NOT NULL,
            planettype INT NOT NULL,
            color VARCHAR(7) NOT NULL,
            size INT NOT NULL,
            name VARCHAR(15))", array());

        $db->ExecuteStatement("CREATE TABLE IF NOT EXISTS planettypes 
             (id INT NOT NULL PRIMARY KEY auto_increment ,
            typename VARCHAR(20))", array());

        //Dodawanie kluczy obcych

        $db->ExecuteStatement("ALTER TABLE playerships 
        ADD CONSTRAINT fk_user_id FOREIGN KEY (userid) REFERENCES users(id);
        ",array());

        $db->ExecuteStatement("ALTER TABLE playerships 
        ADD CONSTRAINT fk_system_id FOREIGN KEY (systemid) REFERENCES coords(id);
        ", array());

        $db->ExecuteStatement("ALTER TABLE playerships 
        ADD CONSTRAINT fk_shiptype FOREIGN KEY (shiptype) REFERENCES shiptypes(id);
        ", array());




        //Uzupełnianie wartości

        $db->ExecuteStatement("INSERT INTO planettypes VALUES (,'Earth-like'),(,'Gas'),(,'Oceanic'),(,'Ice'),(,'Lava'),(,'Urbanized')");





        $values = "";

        for($i = 0; $i<WORLD_SYSTEMS_AMOUNT;$i++)
        {
                $values.= "(null,".rand(0,WORLD_SIZE_MAX).",".rand(0, WORLD_SIZE_MAX)."),";
        }

        $values[strlen($values)-1]=';';
        
        $db->ExecuteStatement("INSERT IGNORE INTO coords VALUES $values");

        $values = "";

        for ($i = 0; $i < WORLD_SYSTEMS_AMOUNT; $i++) {
            $val = rand(1,9);
            for($j = 0 ; $j < $val ; $j++)
            {
            $values .= "(null," . $i . "," . rand(1, 5) . ","."'#".str_pad(dechex(mt_rand(0,0xFFFFFF)),6,'0',STR_PAD_LEFT)."',". rand(20, 100).",''),";
            }
        }

        $values[strlen($values) - 1] = ';';

        $db->ExecuteStatement("INSERT IGNORE INTO planets VALUES $values");




        $db->ExecuteStatement("
        DELETE t1 FROM coords t1 INNER JOIN coords t2
         WHERE t1.id < t2.id AND t1.x = t2.x AND t1.y = t2.y;
        ");



?>