<?php

class GlobalMapController extends Controller
{
public function getmapdata()
{
    $this->nopage = true;
    $this->model->getMapData($_SESSION["userid"]);
}

    public function getplayerdata()
    {
        $this->nopage = true;
        $this->model->getPlayerData($_SESSION["userid"]);
    }

    public function flyToOtherSystem($destinationId)
    {
        $this->nopage = true;
        $this->model->flyToOtherSystem($_SESSION["userid"],$destinationId);
    }

    public function getSystemData($systemid)
    {
        $this->nopage = true;
        $this->model->getSystemData($_SESSION["userid"],$systemid);
    }

}

?>