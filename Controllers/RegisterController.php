<?php

class RegisterController extends Controller
{

    public function send()
    {
        $login = $_POST["login"];
        $passwd = $_POST["password"];
        $email = $_POST["email"];
        $this->model->register($login,$passwd,$email);

        if ($this->model->IsSuccess())
        {
                header("Location: /login/registered");
        }
    }
}
?>