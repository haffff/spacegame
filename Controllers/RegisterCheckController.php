<?php
class RegisterCheckController extends Controller
{

    public function __construct(Model $model, string $name)
    {
        parent::__construct($model,$name);
        $this->nopage = true;
    }

    public function login($login)
    {
        $this->model->login($login);
    }

    public function email($email)
    {
        $this->model->email($email);
    }
}
?>