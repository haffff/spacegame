<?php

class Controller
{
    public function __construct(Model $model,string $name)
    {
        $this->model = $model;
        $this->name = $name;
        $this->nopage = false;
    }

    public function Render()
    {
        if($this->nopage)
        {
        require_once "../Views/JsondefaultView.php";
        }
        else
        {
        require_once "../Views/$this->name"."View.php";
        }
    }

    public function RenderLeftPanel()
    {
        
        if(isset($_SESSION['userid']))
        {
           
            require_once "../Views/LeftPanel/DefaultLoggedView.php";
        }
        else 
        {
            
            require_once "../Views/LeftPanel/DefaultGuestView.php";
        }
    }

    public function RenderTopPanel()
    {
        if (isset($_SESSION['userid'])) {
            require_once "../Views/TopPanel/DefaultLoggedView.php";
        } else {
            require_once "../Views/TopPanel/DefaultGuestView.php";
        }
    }
}


?>