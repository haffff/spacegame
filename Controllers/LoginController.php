<?php

class LoginController extends Controller
{

    public function logout()
    {
        session_destroy();
        header("Location: /login");
    }

    public function registered()
    {
        $this->model->registered = true;
    }

    public function send()
    {
        $login = $_POST["login"];
        $passwd = $_POST["password"];

        $this->model->CheckLogin($login);
        $this->model->CheckPassword($login, $passwd);

        $loginres = $this->model->GetCheckLoginResult();
        $passwordres = $this->model->GetCheckPasswordResult();
        if ($loginres)
        {
            if ($passwordres)
            {
                
                session_start();
                $row = $this->model->GetResult();
                $_SESSION["userid"] = $row["id"];
                $_SESSION["login"] = $login;

                if ($row["admin"] == 1) {
                    $_SESSION["admin"] = true;
                } else {
                    $_SESSION["admin"] = false;
                }

               header("Location: /main");


            }
        }
    }

    




}

?>